wDY<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"846";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-11-18 20:03:59";s:13:"post_date_gmt";s:19:"2016-11-18 23:03:59";s:12:"post_content";s:1751:"<h2>Pai fotógrafo realiza o sonho da sua filha com um ensaio fotográfico da mulher maravilha.</h2>

<h2><img class="aligncenter wp-image-861 size-full" src="https://www.gracieledombek.com.br/wp-content/uploads/2016/11/WW1SMALL-1-1920x1051.jpg" alt="Ensaio Fotográfico Mulher Maravilha - 19" width="1920" height="1051" /></h2>

Quem nunca ouviu seu filho(a) dizer que gostaria de ser o super herói mais poderoso de todos os tempos ou já viu seu filho batendo aquele papo com o amigo imaginário?

A imaginação está relacionada à criatividade e é de extrema importância para o desenvolvimento do ser humano. Atualmente, vivemos em um mundo onde os criativos são os que se sobressaem e, por isso, estimular a imaginação de uma criança é tão importante. Cabe a nós, adultos, pararmos para escutá-los, questioná-los e ajudá-los com todos os pensamentos que rodeiam a cabecinha dos baixinhos.

Semana passada li uma matéria de um fotógrafo fantástico que realizou o sonho da sua filha de se transformar na super heroína mais poderosa de todos os tempos. Baseado no trailer do filme "A Mulher Maravilha", que será lançado em 2017, Josh Rossi preparou para sua pequena um ensaio fotográfico temático super produzido e com efeitos especiais de tirar o fôlego. Confira.

[gallery columns="1" link="file" size="full" ids="852,848,847,849,859"]

<h2>Veja o antes e depois do ensaio fotográfico da mulher maravilha:</h2>

[gallery columns="1" link="file" size="full" ids="854,853,850,851"]

Fotos:  <a href="http://joshrossi.com/">Josh Rossi</a>

Confira no vídeo abaixo, cenas dos bastidores de toda a produção envolvida no ensaio temático dessa linda heroína. =)

https://www.youtube.com/watch?v=7WW5l67xBqM

<h2></h2>";s:10:"post_title";s:67:"Ensaio Fotográfico da Mulher Maravilha: pai realiza sonho da filha";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:35:"ensaio-fotografico-mulher-maravilha";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2016-11-19 21:51:31";s:17:"post_modified_gmt";s:19:"2016-11-20 00:51:31";s:21:"post_content_filtered";s:1762:"<h2>Pai fotógrafo realiza o sonho da sua filha com um ensaio fotográfico da mulher maravilha.</h2>
<h2><img class="aligncenter wp-image-861 size-full" src="http://www.gracieledombek.com.br/wp-content/uploads/2016/11/WW1SMALL-1-1920x1051.jpg" alt="Ensaio Fotográfico Mulher Maravilha - 19" width="1920" height="1051" /></h2>
Quem nunca ouviu seu filho(a) dizer que gostaria de ser o super herói mais poderoso de todos os tempos ou já viu seu filho batendo aquele papo com o amigo imaginário?

A imaginação está relacionada à criatividade e é de extrema importância para o desenvolvimento do ser humano. Atualmente, vivemos em um mundo onde os criativos são os que se sobressaem e, por isso, estimular a imaginação de uma criança é tão importante. Cabe a nós, adultos, pararmos para escutá-los, questioná-los e ajudá-los com todos os pensamentos que rodeiam a cabecinha dos baixinhos.

Semana passada li uma matéria de um fotógrafo fantástico que realizou o sonho da sua filha de se transformar na super heroína mais poderosa de todos os tempos. Baseado no trailer do filme "A Mulher Maravilha", que será lançado em 2017, Josh Rossi preparou para sua pequena um ensaio fotográfico temático super produzido e com efeitos especiais de tirar o fôlego. Confira.

[gallery columns="1" link="file" size="full" ids="852,848,847,849,859"]
<h2>Veja o antes e depois do ensaio fotográfico da mulher maravilha:</h2>
[gallery columns="1" link="file" size="full" ids="854,853,850,851"]

Fotos:  <a href="http://joshrossi.com/">Josh Rossi</a>

Confira no vídeo abaixo, cenas dos bastidores de toda a produção envolvida no ensaio temático dessa linda heroína. =)

https://www.youtube.com/watch?v=7WW5l67xBqM
<h2></h2>";s:11:"post_parent";s:1:"0";s:4:"guid";s:40:"https://www.gracieledombek.com.br/?p=846";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}